# Welcome to React + Redux with TypeScript Class!

## Instructor

Eric Greene - [http://t4d.io](http://t4d.io)

## Schedule

Class:

- Monday through Friday: 9am to 5pm

Breaks:

- Morning Break: 10:25am to 10:35am
- Lunch: 12pm to 1pm
- Afternoon Break #1: 2:15pm to 2:25pm
- Afternoon Break #2: 3:40pm to 3:50pm

## Course Outline

- Day 1 - What is React, Functional Components, JSX, Props, Default Props, Class Components, State
- Day 2 - Composition (including containment + specialization), CSS with React + Portals
- Day 3 - Redux, React-Redux (including discussion on higher order components)
- Day 4 - Asychronous Programming, Promises, Async/Await, Redux Thunk
- Day 5 - Other React Topics, Discussion of RxJS and GraphQL

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [TypeScript](https://www.typescriptlang.org/)