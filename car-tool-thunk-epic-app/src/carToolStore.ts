import { createStore, combineReducers, applyMiddleware, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';
import thunk from 'redux-thunk';

import { actionFromClass } from './middleware/actionFromClass.middleware';

import { CarToolState } from './CarToolState';

import { carsReducer } from './reducers/cars.reducer';
import { editCarIdReducer } from './reducers/editCarId.reducer';
import { loadingReducer } from './reducers/loading.reducer';
import { errorMessageReducer } from './reducers/errorMessage.reducer';

import { carEpic } from './actions/car.epics';


const rootReducer = combineReducers({
  cars: carsReducer,
  editCarId: editCarIdReducer,
  loading: loadingReducer,
  errorMessage: errorMessageReducer,
});

const epicMiddleware = createEpicMiddleware();

export const carToolStore: Store<CarToolState> = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(epicMiddleware, actionFromClass))
  // composeWithDevTools(applyMiddleware(thunk, actionFromClass)),
);



epicMiddleware.run(carEpic);