import { Action } from 'redux';

import { ErrorActions, RaiseErrorAction } from '../actions/error.actions';

export const errorMessageReducer = (state = '', action: Action<string>) => {

  if (action.type === ErrorActions.RAISE_ERROR) {
    return (action as RaiseErrorAction).errorMessage;
  }

  return '';

};