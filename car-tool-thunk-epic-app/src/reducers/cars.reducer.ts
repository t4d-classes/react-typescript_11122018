import { Action } from 'redux';

import { Car } from '../models/Car';
import { CarActions, RefreshCarsDoneAction } from '../actions/car.actions';

export const carsReducer = (state: Car[] = [], action: Action<string>) => {

  if (action.type === CarActions.REFRESH_CARS_DONE) {
    return (action as RefreshCarsDoneAction).payload;
  }

  return state;
};