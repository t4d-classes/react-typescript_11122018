import { Action } from 'redux';

import { CarActions, EditCarAction } from '../actions/car.actions';

export const editCarIdReducer = (state = -1, action: Action<string>) => {

  if (action.type === CarActions.EDIT_CAR) {
    return (action as EditCarAction).payload;
  }

  return -1;

};