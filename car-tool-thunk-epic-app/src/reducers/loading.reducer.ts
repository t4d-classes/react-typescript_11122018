import { Action } from 'redux';

export const loadingReducer = (state = false, action: Action<string>) => {

  if (action.type.endsWith('REQUEST')) {
    return true;
  }

  if (action.type.endsWith('DONE')) {
    return false;
  }

  return state;
};