import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import './index.css';

import { CarToolContainer } from './containers/CarToolContainer';
import { ErrorBannerContainer } from './containers/ErrorBannerContainer';
import { carToolStore } from './carToolStore';

ReactDOM.render(
  <Provider store={carToolStore}>
    <div>
      <ErrorBannerContainer />
      <CarToolContainer />
    </div>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
