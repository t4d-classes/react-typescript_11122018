import * as React from 'react';

import { Car } from '../models/Car';

export interface Props {
  car: Car;
  onDelete: (carId: number) => void;
  onEdit: (carId: number) => void;
}

export const ViewCarRow = ({ car, onDelete, onEdit }: Props) => {

  return <tr>
    <td>{car.id}</td>
    <td>{car.make}</td>
    <td>{car.model}</td>
    <td>{car.year}</td>
    <td>{car.color}</td>
    <td>{car.price}</td>
    <td>
      <button type="button" onClick={() => onEdit(car.id as number)}>Edit</button>
      <button type="button" onClick={() => onDelete(car.id as number)}>Delete</button>
    </td>
  </tr>;

};