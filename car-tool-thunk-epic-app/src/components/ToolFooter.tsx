import * as React from 'react';

export interface Props {
  companyName: string;
}

export const ToolFooter = ({ companyName }: Props) => {

  return <footer>
    <small>&copy; {new Date().getFullYear()}, {companyName}</small>
  </footer>;
}