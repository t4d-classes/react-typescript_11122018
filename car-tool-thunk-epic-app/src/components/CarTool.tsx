import * as React from 'react';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';
import { ToolFooter } from './ToolFooter';

import { Car } from '../models/Car';

export interface Props {
  cars: Car[];
  editCarId: number;
  loading: boolean;
  onRefresh: () => void;
  onAppend: (car: Car) => void;
  onReplace: (car: Car) => void;
  onDelete: (carId: number) => void;
  onEdit: (carId: number) => void;
  onCancel: () => void;
}

export class CarTool extends React.Component<Props> {

  public componentDidMount() {
    this.props.onRefresh();
  }
  
  public render() {
    return <div>
      <ToolHeader headerText="Car Tool" />
      <button type="button" onClick={this.props.onRefresh}>Refresh</button>
      <CarTable cars={this.props.cars} editCarId={this.props.editCarId}
        onEdit={this.props.onEdit} onDelete={this.props.onDelete}
        onSave={this.props.onReplace} onCancel={this.props.onCancel} />
      <CarForm buttonText="Add Car" onSubmit={this.props.onAppend} />
      <ToolFooter companyName="A Cool Company, Inc." />
    </div>;
  }

}