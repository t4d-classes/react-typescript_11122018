import * as React from 'react';

import { Car } from '../models/Car';

import { ViewCarRow } from './ViewCarRow';
import { EditCarRow } from './EditCarRow';

export interface CarTableProps {
  cars: Car[];
  editCarId: number;
  onEdit: (carId: number) => void;
  onDelete: (carId: number) => void;
  onSave: (car: Car) => void;
  onCancel: () => void;
}

export const CarTable = ({ cars, editCarId, onEdit, onDelete, onSave, onCancel }: CarTableProps) => {

  return <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>Color</th>
        <th>Price</th>
        <th>Actions</th>
     </tr>
    </thead>
    <tbody>
      {cars.map(car => car.id === editCarId
        ? <EditCarRow key={car.id} car={car} onSave={onSave} onCancel={onCancel} />
        : <ViewCarRow key={car.id} car={car} onEdit={onEdit} onDelete={onDelete} />)}
    </tbody>
  </table>;
}