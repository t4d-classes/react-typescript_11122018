import * as React from 'react';

import { Color } from '../models/Color';
import { change } from '../utils/change';

export interface Props {
  onSubmit: (color: Color) => void;
  buttonText: string;
}

export interface State {
  color: string;
  hexcode: string;
  [ x: string ]: any;
}

export class ColorForm extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      color: '',
      hexcode: '',
    };
  }

  public submitColor = () => {
    this.props.onSubmit({
      color: this.state.color,
      hexCode: this.state.hexcode,
    });

    this.setState({
      color: '',
      hexcode: '',
    });
  };

  public render() {
    return <form>
      <div>
        {/* React.createElement('label', { htmlFor: 'color-input'}, 'Color:'); */}
        <label htmlFor="color-input">Color:</label>
        <input type="text" name="color" id="color-input"
          value={this.state.color} onChange={change(this)} />
      </div>
      <div>
        <label htmlFor="hexcode-input">HexCode</label>
        <input type="text" name="hexcode" id="hexcode-input"
          value={this.state.hexcode} onChange={change(this)} />
      </div>

      <button type="button" onClick={this.submitColor}>{this.props.buttonText}</button>

    </form>;
  }
}