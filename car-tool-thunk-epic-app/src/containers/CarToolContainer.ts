import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { CarToolState } from '../CarToolState';

import { CarTool } from '../components/CarTool';

import { actions } from '../actions/car.epics';
// import { actions } from '../actions/car.thunks';

const mapStateToProps = ({ loading, cars, editCarId }: CarToolState) => ({ loading, cars, editCarId });

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  onRefresh: actions.refreshCars,
  onAppend: actions.appendCar,
  onReplace: actions.replaceCar,
  onDelete: actions.deleteCar,
  onEdit: actions.editCar,
  onCancel: actions.cancelCar,
}, dispatch);

export const CarToolContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CarTool);