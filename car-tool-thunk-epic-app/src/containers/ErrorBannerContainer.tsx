import * as React from 'react';
import { connect } from 'react-redux';
import { CarToolState } from '../CarToolState';

export interface Props {
  errorMessage: string;
}

export const ErrorBanner = ({ errorMessage }: Props) => {

  return <div>{errorMessage}</div>;

};

export const ErrorBannerContainer = connect(
  ({ errorMessage }: CarToolState) => ({ errorMessage }),
)(ErrorBanner);