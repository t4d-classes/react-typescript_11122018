import { Dispatch } from 'redux';

import { Car } from '../models/Car';
import { CarToolState } from '../CarToolState';
import {
  RefreshCarsRequestAction, RefreshCarsDoneAction, EditCarAction, CancelCarAction,
  AppendCarRequestAction, ReplaceCarRequestAction, DeleteCarRequestAction
} from './car.actions';
import { RaiseErrorAction } from './error.actions';

export const refreshCars = () => {

  return (dispatch: Dispatch<any>, getState: () => CarToolState) => {

    console.log(getState());

    dispatch(new RefreshCarsRequestAction());
    return fetch('http://localhost:3050/cars')
      .then(res => res.json())
      .then((cars: Car[]) => dispatch(new RefreshCarsDoneAction(cars)))
      .catch(err => dispatch(new RaiseErrorAction(err.message)));
  };

};

export const appendCar = (car: Car) => {

  return (dispatch: Dispatch<any>, getState: () => CarToolState) => {

    dispatch(new AppendCarRequestAction(car));
    return fetch('http://localhost:3050/cars', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    })
      .then(() => dispatch(refreshCars()))
      .catch(err => dispatch(new RaiseErrorAction(err.message)));
  };

};

export const replaceCar = (car: Car) => {

  return (dispatch: Dispatch<any>, getState: () => CarToolState) => {

    if (!car.id) {
      dispatch(new RaiseErrorAction('car requires an id'));
      return;
    }

    const encodedCarId = encodeURIComponent(car.id.toString());

    dispatch(new ReplaceCarRequestAction(car));
    return fetch(`http://localhost:3050/cars/${encodedCarId}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    })
      .then(() => dispatch(refreshCars()))
      .catch(err => dispatch(new RaiseErrorAction(err.message)));
  };

};

export const deleteCar = (carId: number) => {

  return (dispatch: Dispatch<any>, getState: () => CarToolState) => {

    dispatch(new DeleteCarRequestAction(carId));
    return fetch(`http://localhost:3050/cars/${encodeURIComponent(carId.toString())}`, {
      method: 'DELETE',
    })
      .then(() => dispatch(refreshCars()))
      .catch(err => dispatch(new RaiseErrorAction(err.message)));
  };

};

export const actions = {
  refreshCars,
  appendCar,
  replaceCar,
  deleteCar,
  editCar: (carId: number) => new EditCarAction(carId),
  cancelCar: () => new CancelCarAction(),
}