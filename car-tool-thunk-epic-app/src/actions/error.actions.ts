import { Action } from 'redux';

export enum ErrorActions {
  RAISE_ERROR = 'RAISE_ERROR',
}

export class RaiseErrorAction implements Action<string> {
  public readonly type = ErrorActions.RAISE_ERROR;
  constructor(public errorMessage: string) { }
} 