import { Action } from 'redux';

import { Car } from '../models/Car';

export enum CarActions {
  REFRESH_CARS_REQUEST = 'REFRESH_CARS_REQUEST',
  REFRESH_CARS_DONE = 'REFRESH_CARS_DONE',
  APPEND_CAR_REQUEST = 'APPEND_CAR_REQUEST',
  APPEND_CAR_DONE = 'APPEND_CAR_DONE',
  REPLACE_CAR_REQUEST = 'REPLACE_CAR_REQUEST',
  REPLACE_CAR_DONE = 'REPLACE_CAR_DONE',
  DELETE_CAR_REQUEST = 'DELETE_CAR_REQUEST',
  DELETE_CAR_DONE = 'DELETE_CAR_DONE',
  EDIT_CAR = 'EDIT_CAR',
  CANCEL_CAR = 'CANCEL_CAR',
}

export class RefreshCarsRequestAction implements Action<string> {
  public readonly type = CarActions.REFRESH_CARS_REQUEST;
}

export class RefreshCarsDoneAction implements Action<string> {
  public readonly type = CarActions.REFRESH_CARS_DONE;
  constructor(public payload: Car[]) { } 
}

export class AppendCarRequestAction implements Action<string> {
  public readonly type = CarActions.APPEND_CAR_REQUEST;
  constructor(public payload: Car) { }
}

export class ReplaceCarRequestAction implements Action<string> {
  public readonly type = CarActions.REPLACE_CAR_REQUEST;
  constructor(public payload: Car) { }
}

export class DeleteCarRequestAction implements Action<string> {
  public readonly type = CarActions.DELETE_CAR_REQUEST;
  constructor(public payload: number) { }
}

export class EditCarAction implements Action<string> {
  public readonly type = CarActions.EDIT_CAR;
  constructor(public payload: number) {}
}

export class CancelCarAction implements Action<string> {
  public readonly type = CarActions.CANCEL_CAR;
}
