import { Action } from 'redux';
import { ofType, combineEpics } from 'redux-observable';
import { Observable, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, switchMap, mapTo, catchError, mergeMap, withLatestFrom } from 'rxjs/operators';

import { CarToolState } from '../CarToolState';
import { Car } from '../models/Car';
import {
  CarActions, RefreshCarsDoneAction, RefreshCarsRequestAction, AppendCarRequestAction,
  EditCarAction, CancelCarAction, ReplaceCarRequestAction, DeleteCarRequestAction,
} from './car.actions';
import { RaiseErrorAction  } from './error.actions';

export const refreshCarsEpic = (
  action$: Observable<Action<string>>,
  state$: Observable<CarToolState>
) =>
  action$.pipe(
    ofType(CarActions.REFRESH_CARS_REQUEST),
    withLatestFrom(state$),
    switchMap(([ action, state ]) => {

      console.log(action);
      console.log(state);

      return ajax('http://localhost:3050/cars').pipe(
        map(ajaxResponse => ajaxResponse.response),
        map((cars: Car[]) => new RefreshCarsDoneAction(cars)),
        catchError(() => of(new RaiseErrorAction('Refresh failed')))
      );
    }),
  );

export const appendCarEpic = (action$: Observable<Action<string>>) =>
  action$.pipe(
    ofType(CarActions.APPEND_CAR_REQUEST),
    mergeMap((action: AppendCarRequestAction) => {
      return ajax.post(
        'http://localhost:3050/cars',
        JSON.stringify(action.payload),
        { 'Content-Type': 'application/json'},
      ).pipe(
        mapTo(new RefreshCarsRequestAction()),
        catchError(err => of(new RaiseErrorAction(err.message))),
      );
    }),
  );

export const replaceCarEpic = (action$: Observable<Action<string>>) =>
  action$.pipe(
    ofType(CarActions.REPLACE_CAR_REQUEST),
    mergeMap((action: ReplaceCarRequestAction) => {

      if (!action.payload.id) {
        return of(new RaiseErrorAction('car requires an id'));
      }
  
      const encodedCarId = encodeURIComponent(action.payload.id.toString());
  
      return ajax.put(
        `http://localhost:3050/cars/${encodedCarId}`,
        JSON.stringify(action.payload),
        { 'Content-Type': 'application/json'},
      ).pipe(
        mapTo(new RefreshCarsRequestAction()),
        catchError(err => of(new RaiseErrorAction(err.message))),
      );
    }),
  );

export const deleteCarEpic = (action$: Observable<Action<string>>) =>
  action$.pipe(
    ofType(CarActions.DELETE_CAR_REQUEST),
    mergeMap((action: DeleteCarRequestAction) => {
      return ajax.delete(
        `http://localhost:3050/cars/${encodeURIComponent(action.payload.toString())}`,
      ).pipe(
        mapTo(new RefreshCarsRequestAction()),
        catchError(err => of(new RaiseErrorAction(err.message))),
      );
    }),
  );

export const carEpic = combineEpics(
  refreshCarsEpic,
  appendCarEpic,
  replaceCarEpic,
  deleteCarEpic,
);

export const refreshCars = () => new RefreshCarsRequestAction();
export const appendCar = (car: Car) => new AppendCarRequestAction(car);
export const replaceCar = (car: Car) => new ReplaceCarRequestAction(car);
export const deleteCar = (carId: number) => new DeleteCarRequestAction(carId);
export const editCar = (carId: number) => new EditCarAction(carId);
export const cancelCar = () => new CancelCarAction();

export const actions = {
  refreshCars,
  appendCar,
  replaceCar,
  deleteCar,
  editCar,
  cancelCar,
};

