Exercise 2

1. Create a new component named Car Tool in the components folder.

2. In Car Tool, utilize Tool Header to display the header "<YOUR NAME>'s Car Lot". Pass the header as a prop.

3. Utilize the new Car Tool component in index.tsx replace the Tool Header component.

4. Create a new component named Tool Footer in the components folder.

5. Tool Footer accepts a single prop of "companyName". And displays the following:

Copyright <CURRENT YEAR>, <COMPANY NAME>

Both current year and company name are JSX expression.

6. Utilize Tool Footer inside of Car Tool component. Display it beneath the Tool Header.

7. Solve world hunger.

8. Solve world peace.

9. Ensure it works.

