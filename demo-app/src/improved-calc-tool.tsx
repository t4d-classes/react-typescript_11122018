import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { calcToolStore } from './calcToolStore';
import { CalcToolContainer } from './containers/CalcToolContainer';
import { CalcHistoryContainer } from './containers/CalcHistoryContainer';

ReactDOM.render(
  <Provider store={calcToolStore}>
    <>
      <CalcToolContainer />
      <CalcHistoryContainer />
    </>
  </Provider>,
  document.querySelector('#root'),
);

