import * as React from 'react';

export interface Action<T = any> {
  type: T;
}

export type State = any;

export type Reducer<S extends State, A extends Action> =
  (state: S, action: A) => S;

export type Subscribe = () => void;
export type Unsubscribe = () => void;

export interface Store<S extends State> {
  getState: () => S;
  dispatch: (action: Action) => void;
  subscribe: (fn: Subscribe) => Unsubscribe;
}

export type CreateStore = <S, A extends Action>(reducer: Reducer<S, A>) => Store<S>;

export const createStore: CreateStore = <S, A extends Action>(reducer: Reducer<S, A>) => {

  let currentState: S;
  const subscribers: Subscribe[] = [];

  // is to trigger the initial rendering of the component
  setTimeout(() => {
    currentState = reducer.apply(null, [undefined, { type: '@INIT' }]);
    subscribers.forEach(fn => fn());
  }, 0);
  
  return {
    getState: () => currentState,
    dispatch: (action: A) => {
      currentState = reducer(currentState, action);
      subscribers.forEach(fn => fn());
    },
    subscribe: (fn: Subscribe) => {
      subscribers.push(fn);
      return () => {
        const index = subscribers.findIndex(fn2 => fn2 === fn);
        subscribers.splice(index, 1);
      };
    },
  };

};

export interface ActionMap {
  [x: string]: (...params: any[]) => Action;
}

export type BoundActionsMap = ActionMap;

export type Dispatch = (action: Action) => void; 

export const bindActionCreators = (actions: ActionMap, dispatch: Dispatch) => {
  return Object.keys(actions).reduce( (boundActions, actionKey) => {
    boundActions[actionKey] = (...params: any[]) => dispatch( actions[actionKey](...params) );
    return boundActions;
  }, {}) as BoundActionsMap;
};

export interface ContainerProps {
  store: Store<State>;
}

export const { Provider, Consumer } = React.createContext({});

export const connect = (
  mapStateToProps: (state: State) => object,
  mapDispatchToProps: (dispatch: Dispatch) => BoundActionsMap) => {
  return (PresentationalComponent: React.ComponentClass) => {
    class ContainerComponent extends React.Component<ContainerProps> {

      public dispatchProps: BoundActionsMap;
      public unsubscribe: Unsubscribe;

      constructor(props: ContainerProps) {
        super(props);

        this.dispatchProps = mapDispatchToProps(props.store.dispatch);
      }

      public componentDidMount() {
        this.unsubscribe = this.props.store.subscribe(() => {
          this.forceUpdate();
        });
      }

      public componentWillUnmount() {
        this.unsubscribe();
      }

      public render() {
        return <PresentationalComponent {...this.dispatchProps} {...mapStateToProps(this.props.store.getState())}  />;
      }
    }

    return () => <Consumer>
      {(value: Store<State>) => <ContainerComponent store={value} />}
    </Consumer>;
  };
};

