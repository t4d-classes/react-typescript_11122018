import { Action } from 'redux';

export enum CalcActions {
  ADD = '[Calc] ADD',
  SUBTRACT = '[Calc] SUBTRACT',
  MULTIPLY = '[Calc] MULTIPLY',
}

export class AddAction implements Action<string> {
  public readonly type = CalcActions.ADD;
  constructor(public payload: number) { }
}

export class SubtractAction implements Action<string> {
  public readonly type = CalcActions.SUBTRACT;
  constructor(public payload: number) { }
}

export class MultiplyAction implements Action<string> {
  public readonly type = CalcActions.MULTIPLY;
  constructor(public payload: number) { }
}

export type CalcActionsUnion = AddAction | SubtractAction | MultiplyAction;
