import { createStore, Store, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import { CarToolState } from './models/CarToolState';
import { carsReducer } from './reducers/cars.reducer';
import { editCarIdReducer } from './reducers/editCarId.reducer';
import { actionFromClassMiddleware } from './middleware/actionFromClass';

export const carToolStore: Store<CarToolState> = createStore(
  combineReducers({
    cars: carsReducer,
    editCarId: editCarIdReducer,
  }),
  composeWithDevTools(applyMiddleware(actionFromClassMiddleware)),
);