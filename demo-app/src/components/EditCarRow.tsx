import * as React from 'react';
import { omit } from 'lodash';

import { Car } from '../models/Car';
import { CarForm } from '../models/CarForm';
import { change } from '../utils/change';

export interface Props {
  car: Car;
  onSave: (car: Car) => void;
  onCancel : () => void;
}

// export interface State {
//   make: string;
//   model: string;
//   year: number;
//   color: string;
//   price: number;
// }

export type State = CarForm;

export class EditCarRow extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      ...omit(props.car, ["id"]),
    };
  }

  public saveCar = () => {
    this.props.onSave({
      id: this.props.car.id,
      ...this.state,
    });
  }

  public render() {
    return <tr>
      <td><input type="text" name="make" value={this.state.make} onChange={change(this)} /></td>
      <td><input type="text" name="model" value={this.state.model} onChange={change(this)} /></td>
      <td><input type="number" name="year" value={this.state.year} onChange={change(this)} /></td>
      <td><input type="text" name="color" value={this.state.color} onChange={change(this)} /></td>
      <td><input type="number" name="price" value={this.state.price} onChange={change(this)} /></td>
      <td>
        <button type="button" onClick={this.saveCar}>Save</button>
        <button type="button" onClick={this.props.onCancel}>Cancel</button>
      </td>
    </tr>;
  }


}