import * as React from 'react';

export interface Props {
  title: string;
  items: string[];
}

export const UnorderedList = ({ title, items }: Props) => {

  return <div>
    <h2>{title}</h2>
    <ul>
      {items.map( (item, index) => <li key={index}>{item}</li>)}
    </ul>
  </div>;

};