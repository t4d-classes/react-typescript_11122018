import * as React from 'react';

import { Color } from '../models/Color';

import { ToolHeader } from './ToolHeader';
import { ColorList } from './ColorList';
import { ColorForm } from './ColorForm';
import { ToolFooter } from './ToolFooter';

export interface Props {
  colors: Color[];
}

export interface State {
  colors: Color[];
}

export class ColorTool extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);

    this.state = {
      colors: props.colors.concat(),
      // possible performance issue: colors: [ ...props.colors ],
    };
  }

  public appendColor = (color: Color) => {

    const ids = this.state.colors.map(c => c.id) as number[];

    const newColor = {
      ...color,
      id: Math.max(...ids, 0) + 1,
    };

    this.setState({
      colors: this.state.colors.concat(newColor),
    });
  };

  public render() {
    return <div>
      <ToolHeader headerText="Color Tool" />
      <ColorList colors={this.state.colors} />
      <ColorForm onSubmit={this.appendColor} buttonText="Add Color" />
      <ToolFooter companyName="A Cool Company" />
    </div>;
  }
  
};