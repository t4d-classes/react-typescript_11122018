import * as React from 'react';

export interface CalcToolProps {
  result: number;
  onAdd: (num: number) => void;
  onSubtract: (num: number) => void;
  onMultiply: (num: number) => void;
};

export interface CalcToolState {
  input: number;
}

export class CalcTool extends React.Component<CalcToolProps, CalcToolState> {

  constructor(props: CalcToolProps) {
    super(props);

    this.state = {
      input: 0,
    };
  }

  public change = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ input: Number(e.target.value) });
  }

  public render() {

    const { result, onAdd, onSubtract, onMultiply } = this.props;

    return <form>
      <div>
        Result: {result}
      </div>
      <div>
        Input: <input type="number" value={this.state.input} onChange={this.change} />
      </div>
      <button type="button" onClick={() => onAdd(this.state.input)}>+</button>
      <button type="button" onClick={() => onSubtract(this.state.input)}>-</button>
      <button type="button" onClick={() => onMultiply(this.state.input)}>*</button>
    </form>;
  }
}
