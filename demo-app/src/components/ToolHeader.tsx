import * as React from 'react';

export interface Props {
  headerText: string;
}

export const ToolHeader = ({ headerText }: Props) => {

  return <header>
    <h1>{headerText}</h1>
  </header>;
}