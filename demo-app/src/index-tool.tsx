// import * as React from 'react';
// import * as ReactDOM from 'react-dom';

// import { Color } from './models/Color';
// import { Car } from './models/Car';

// import { ColorTool } from './components/ColorTool';
// import { CarTool } from './components/CarTool';

// import './index.css';

// const colors: Color[] = [
//   { id: 1, color: 'red', hexCode: '#FF0000' },
//   { id: 2, color: 'green', hexCode: '#00FF00' },
//   { id: 3, color: 'blue', hexCode: '#0000FF' },
// ];

// const cars: Car[] = [
//   { id: 1, make: 'Tesla', model: 'Model S', year: 2018, color: 'red', price: 120000 },
//   { id: 2, make: 'Nissan', model: 'Versa', year: 2014, color: 'blue', price: 14000 },
// ];

// ReactDOM.render(<>
//   <ColorTool colors={colors} />
//   <CarTool cars={cars} />
// </>, document.querySelector('#root') as HTMLElement);

