import { Observable, Observer,
  OperatorFunction
} from 'rxjs';
import { map, filter, tap, delay } from 'rxjs/operators';


const nums: Observable<number> = Observable.create(( observer: Observer<number>) => {

  console.log('called observer function');

  let counter = 0;

  setInterval(() => {
    counter++;
    console.log('emitting counter', counter);
    observer.next(counter);
  }, 1000);
});

const ops: Array<OperatorFunction<any, any>> = [
  delay(4000),
  tap(x => console.log('tap: ', x)),
  map(x => x * 2),
  filter(x => x > 10),
] as Array<OperatorFunction<any, any>>;

const changedNums = nums.pipe.apply(nums, ops);

changedNums.subscribe((num: number) => {
  // console.log(num);
});











// import * as React from 'react';
// import * as ReactDOM from 'react-dom';
// import { Provider } from 'react-redux';

// import { carToolStore } from './carToolStore';
// import { CarToolContainer } from './containers/CarToolContainer';

// import './index.css';

// ReactDOM.render(
//   <Provider store={carToolStore}>
//     <CarToolContainer />
//   </Provider>,
//   document.querySelector('#root') as HTMLElement
// );

