import { createStore, Store, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { CalcToolState } from './models/CalcToolState';
import { resultReducer } from './reducers/result.reducer';
import { historyReducer } from './reducers/history.reducer';
import { actionFromClassMiddleware } from './middleware/actionFromClass';

export const calcToolStore: Store<CalcToolState> = createStore(
  combineReducers({
    result: resultReducer,
    history: historyReducer,
  }),
  composeWithDevTools(applyMiddleware(actionFromClassMiddleware)),
);