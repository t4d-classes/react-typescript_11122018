import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore, bindActionCreators, Store, Action, Reducer, Dispatch, AnyAction, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';

enum CalcActions {
  ADD = 'ADD',
  SUBTRACT = 'SUBTRACT',
  MULTIPLY = 'MULTIPLY',
}

class AddAction implements Action<string> {
  public readonly type = CalcActions.ADD;
  constructor(public payload: number) { }
}

class SubtractAction implements Action<string> {
  public readonly type = CalcActions.SUBTRACT;
  constructor(public payload: number) { }
}

class MultiplyAction implements Action<string> {
  public readonly type = CalcActions.MULTIPLY;
  constructor(public payload: number) { }
}

type CalcActionsUnion = AddAction | SubtractAction | MultiplyAction;

type AppState = number;

const calcReducer: Reducer<AppState, CalcActionsUnion> = (state: AppState = 0, action: CalcActionsUnion) => {
  console.log('state', state, 'action', action);

  switch(action.type) {
    case CalcActions.ADD:
      return state + action.payload;
    case CalcActions.SUBTRACT:
      return state - action.payload;
    case CalcActions.MULTIPLY:
    return state * action.payload;
    default:
      return state;
  }

};


const actionFromClassMiddleware = () => (next: Dispatch<AnyAction>) => (action: Action) => {
  next({ ...action });
}

const store: Store<AppState> = createStore(
  calcReducer,
  composeWithDevTools(applyMiddleware(actionFromClassMiddleware)),
);

interface CalcToolProps {
  result: number;
  onAdd: (num: number) => void;
  onSubtract: (num: number) => void;
  onMultiply: (num: number) => void;
};

interface CalcToolState {
  input: number;
}

class CalcTool extends React.Component<CalcToolProps, CalcToolState> {

  constructor(props: CalcToolProps) {
    super(props);

    this.state = {
      input: 0,
    };
  }

  public change = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ input: Number(e.target.value) });
  }

  public render() {

    const { result, onAdd, onSubtract, onMultiply } = this.props;

    return <form>
      <div>
        Result: {result}
      </div>
      <div>
        Input: <input type="number" value={this.state.input} onChange={this.change} />
      </div>
      <button type="button" onClick={() => onAdd(this.state.input)}>+</button>
      <button type="button" onClick={() => onSubtract(this.state.input)}>-</button>
      <button type="button" onClick={() => onMultiply(this.state.input)}>*</button>
    </form>;
  }
}

const createCalcToolContainer = connect(
  (state) => ({ result: state }), // mapStateToProps
  (dispatch) => bindActionCreators({
    onAdd: (num: number) => new AddAction(num),
    onSubtract: (num: number) => new SubtractAction(num),
    onMultiply: (num: number) => new MultiplyAction(num),
  }, dispatch), // mapDispatchToProps
);

const CalcToolContainer = createCalcToolContainer(CalcTool);

ReactDOM.render(
  <Provider store={store}>
    <div>
      <CalcToolContainer />
      <div>
        <CalcToolContainer />
      </div>      
    </div>
  </Provider>,
  document.querySelector('#root'),
);

