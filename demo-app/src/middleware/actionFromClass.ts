import { Dispatch, Action }  from 'redux';

export const actionFromClassMiddleware = () =>
  (next: Dispatch<Action<string>>) =>
    (action: Action<string>) => {
      next({ ...action });
    };