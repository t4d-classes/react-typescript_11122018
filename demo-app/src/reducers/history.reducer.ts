import { CalcHistory } from '../models/CalcHistory';
import { CalcActions, CalcActionsUnion } from '../actions/calc.actions';

export const historyReducer = (state: CalcHistory[] = [], action: CalcActionsUnion) => {

  switch(action.type) {
    case CalcActions.ADD:
    case CalcActions.SUBTRACT:
    case CalcActions.MULTIPLY:
      return state.concat({ op: action.type, value: action.payload });
    default:
      return state;
  }
};