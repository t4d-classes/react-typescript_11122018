import { Reducer } from 'redux';
import { CalcActionsUnion, CalcActions } from '../actions/calc.actions';

export const resultReducer: Reducer<number, CalcActionsUnion> =
  (state: number = 0, action: CalcActionsUnion) => {
    switch(action.type) {
      case CalcActions.ADD:
        return state + action.payload;
      case CalcActions.SUBTRACT:
        return state - action.payload;
      case CalcActions.MULTIPLY:
      return state * action.payload;
      default:
        return state;
    }
  };