export interface CarForm {
  make: string;
  model: string;
  year: number;
  color: string;
  price: number;
}