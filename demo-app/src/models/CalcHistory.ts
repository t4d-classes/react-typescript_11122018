export interface CalcHistory {
  op: string;
  value: number;
}