import * as React from 'react';
import { shallow, configure, ShallowWrapper } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { ColorForm, Props, State } from './color-form';

configure({ adapter: new Adapter() });

jest.unmock('./color-form');

describe('<ColorForm /> Shallow with Enzyme', () => {

  const colorValue = 'purple';
  const eventHandlers = { submitColor: () => null };

  let component: ShallowWrapper;
  let submitColorSpy: jest.SpyInstance;
  let renderSpy: jest.SpyInstance;
  let onClickSpy: jest.SpyInstance;

  beforeEach(() => {
    submitColorSpy = jest.spyOn(eventHandlers, 'submitColor');
    // use this approach for true class functions
    renderSpy = jest.spyOn(ColorForm.prototype, 'render');
    component = shallow(<ColorForm buttonText="Save Color"
      onSubmitColor={eventHandlers.submitColor} />);
    // use this approach for class arrow functions
    onClickSpy = jest.spyOn(component.instance() as any, 'submitColor');
  });

  test('<ColorForm /> renders', () => {

    // instance is needed here because onsubmitColor is not passed to the form node
    expect((component.instance().props as Props).onSubmitColor).toBe(submitColorSpy);
    expect((component.instance().props as Props).buttonText).toBe('Save Color');
    expect((component.state() as State).color).toBe('');

    const colorInput = component.find('input');
    expect(colorInput.prop('value')).toBe('');

    colorInput.simulate('change', {
      target: { value: colorValue, name: 'color' },
      currentTarget: { value: colorValue }, name: 'color',
    });

    expect((component.state() as State).color).toBe(colorValue);

    component.find('button').simulate('click');

    expect(renderSpy).toHaveBeenCalled();
    expect(onClickSpy).toHaveBeenCalled();
    expect(submitColorSpy).toHaveBeenCalledWith(colorValue);
  });

});