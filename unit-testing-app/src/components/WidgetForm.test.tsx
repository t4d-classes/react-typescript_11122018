import * as React from 'react';
import { render, mount, shallow, ReactWrapper, ShallowWrapper, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { WidgetForm } from './WidgetForm';

configure({ adapter: new Adapter() });

describe('<WidgetForm /> Enzyme Static HTML', () => {

  test('<WidgetForm /> renders', () => {

    const component = JSON.stringify(render(
      <WidgetForm onSubmitWidget={() => null} buttonText="Add Widget" />
    ).html());

    expect(component).toMatchSnapshot();

  });

});

describe('<WidgetForm /> Enzyme Mock DOM', () => {

  const eventHandlers = {
    submitWidget: () => null,
  };

  let submitWidgetSpy:  jest.SpyInstance;
  let component: ReactWrapper;

  beforeEach(() => {

    submitWidgetSpy = jest.spyOn(eventHandlers, 'submitWidget');

    component = mount(
      <WidgetForm onSubmitWidget={eventHandlers.submitWidget} buttonText="Add Widget" />  
    );

  });

  test('<WidgetForm /> renders', () => {

    expect(component.find('button').text()).toBe('Add Widget');

  });

  test('<WidgetForm /> form and submit button', () => {

    const widget = {
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.0,
      quantity: 20,
    };

    Object.keys(widget).forEach(propName => {
      component.find(`#${propName}-input`).simulate('change',
        { target: { value: widget[propName], name: propName } },
      );
    });

    component.find('button').simulate('click');


    expect(submitWidgetSpy).toHaveBeenCalledWith(widget);

  });


});

describe('<WidgetForm /> Enzyme Shallow', () => {

  const eventHandlers = {
    submitWidget: () => null,
  };

  let submitWidgetSpy: jest.SpyInstance;
  let wrapper: ShallowWrapper;

  beforeEach(() => {

    submitWidgetSpy = jest.spyOn(eventHandlers, 'submitWidget');

    wrapper = shallow(
      <WidgetForm onSubmitWidget={eventHandlers.submitWidget} buttonText="Add Widget" />  
    );

  });

  test('<WidgetForm /> renders', () => {

    expect(wrapper.find('button').text()).toBe('Add Widget');

  });

  test('<WidgetForm /> form and submit button', () => {

    const widget = {
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.0,
      quantity: 20,
    };

    Object.keys(widget).forEach(propName => {
      wrapper.find(`#${propName}-input`).simulate('change',
        { target: { value: widget[propName], name: propName } },
      );
    });

    wrapper.find('button').simulate('click');


    expect(submitWidgetSpy).toHaveBeenCalledWith(widget);

  });


});
