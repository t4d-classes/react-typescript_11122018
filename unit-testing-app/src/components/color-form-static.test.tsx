import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { render, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import { ColorForm } from './color-form';

configure({ adapter: new Adapter() });

describe('<ColorForm /> React Test Renderer Static HTML', () => {

  test('<ColorForm /> renders', () => {
    const tree = renderer.create(<ColorForm onSubmitColor={() => null} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

});

describe('<ColorForm /> Enzyme Static HTML', () => {

  test('<ColorForm /> renders', () => {
    const component = JSON.stringify(render(<ColorForm onSubmitColor={() => null} />).html());
    expect(component).toMatchSnapshot();
  });

});
